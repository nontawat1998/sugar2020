from flask import *
import os
from imageai.Detection.Custom import CustomObjectDetection
from pip._vendor import requests

detector = CustomObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath("models/detection_model-ex-003--loss-0008.072.h5")
detector.setJsonPath("json/detection_config.json")
detector.loadModel()

from flask import Flask, render_template, request, redirect, url_for, session
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re

app = Flask(__name__)



# Change this to your secret key (can be anything, it's for extra protection)
app.secret_key = 'your secret key'

# Enter your database connection details below
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'pythonlogin'

# Intialize MySQL
mysql = MySQL(app)

# http://localhost:5000/pythonlogin/ - this will be the login page, we need to use both GET and POST requests
@app.route('/pythonlogin/', methods=['GET', 'POST'])
def login():
    # Output message if something goes wrong...
    msg = ''
    # Check if "username" and "password" POST requests exist (user submitted form)
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        # Create variables for easy access
        username = request.form['username']
        password = request.form['password']
        # Check if account exists using MySQL
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = %s AND password = %s', (username, password,))
        # Fetch one record and return result
        account = cursor.fetchone()
        # If account exists in accounts table in out database
        if account:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
            # Redirect to home page
            return redirect('https://dashboard.tawk.to/#/dashboard/5f7df93df0e7167d001705c1')
        else:
            # Account doesnt exist or username/password incorrect
            msg = 'Incorrect username/password!'
    # Show the login form with message (if any)
    return render_template('login.html', msg=msg)



@app.route("/base")
def base():

    return render_template('base.html')

@app.route("/")
def index():
    return render_template('index.html')

@app.route("/help")
def help():
    return render_template('help.html')

@app.route("/system")
def system():
    return render_template('system.html')

@app.route("/gender")
def gender():
    return render_template('gender.html')

@app.route("/page1")
def page1():
    return render_template('page1.html')

@app.route("/page2")
def page2():
    return render_template('page2.html')

@app.route("/page3")
def page3():
    return render_template('page3.html')

@app.route("/page4")
def page4():
    return render_template('page4.html')

@app.route("/img",methods=['POST'])
def img():
    img = request.files['imgs']

    folder = os.path.join('static','img')
    app.config["IMAGE_UPLOADS"] = folder
    img.save(os.path.join(app.config["IMAGE_UPLOADS"], img.filename))

    detections = detector.detectObjectsFromImage(input_image="static/img/{}".format(img.filename), output_image_path="static/cv/{}".format(img.filename))
    for Project1 in detections:
        print(Project1["name"], " : ", Project1["percentage_probability"], " : ", Project1["box_points"]) 
        return render_template('gender.html',name_img = img.filename, param1 =Project1["name"] ,param2= Project1["percentage_probability"]) 
    
    


if __name__ == "__main__":
    app.run(debug=True)
   